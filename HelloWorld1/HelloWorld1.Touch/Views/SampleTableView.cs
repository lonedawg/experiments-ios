﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using HelloWorld1.Core;

namespace HelloWorld1.Touch
{
	[Register("SampleTableView")]
	public class SampleTableView : MvxTableViewController
	{
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.Title = "Heartbeat";

			var lcSource = new MvxStandardTableViewSource (TableView, "TitleText Name + ' ' + LastResult.ShortResult;");
			TableView.Source = lcSource;

			var lcSet = this.CreateBindingSet<SampleTableView, SampleTableViewModel> ();
			lcSet.Bind (lcSource).To (X => X.Args.Results);
			lcSet.Apply ();

			TableView.ReloadData ();
		}
	}
}

