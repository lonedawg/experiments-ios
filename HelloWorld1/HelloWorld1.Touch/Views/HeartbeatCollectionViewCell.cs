﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using HelloWorld1.Core;

namespace HelloWorld1.Touch
{
	public partial class HeartbeatCollectionViewCell : MvxCollectionViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("HeartbeatCollectionViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("HeartbeatCollectionViewCell");

		public HeartbeatCollectionViewCell (IntPtr handle) : base (String.Empty, handle)
		{
			this.DelayBind (() => {
				var lcSet = this.CreateBindingSet<HeartbeatCollectionViewCell, Thing>();
				lcSet.Bind(NameLabel).To(X => X.Name);
				lcSet.Apply();
			});
		}

		public static HeartbeatCollectionViewCell Create ()
		{
			return (HeartbeatCollectionViewCell)Nib.Instantiate (null, null) [0];
		}
	}
}

