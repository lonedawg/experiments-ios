using System.Drawing;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System;
using Cirrious.MvvmCross.Binding.Touch.Views;
using HelloWorld1.Core;

namespace HelloWorld1.Touch.Views
{
	[Register("SampleCollectionView")]
	public class SampleCollectionView : MvxCollectionViewController
    {
        private Boolean m_Constructed;

		public SampleCollectionView()
			: base (new UICollectionViewFlowLayout
            {
					ItemSize = new SizeF(316, 40)
            })
        {
            m_Constructed = true;
            ViewDidLoad();
        }

        public sealed override void ViewDidLoad()
        {
			if (!m_Constructed)
                return;

			base.ViewDidLoad();

			Title = "Hello!";

			if (RespondsToSelector (new Selector ("edgesForExtendedLayout")))
				EdgesForExtendedLayout = UIRectEdge.None;

			CollectionView.RegisterNibForCell(HeartbeatCollectionViewCell.Nib, HeartbeatCollectionViewCell.Key);
			var lcSource = new MvxCollectionViewSource(CollectionView, HeartbeatCollectionViewCell.Key);
            CollectionView.Source = lcSource;

			var lcSet = this.CreateBindingSet<SampleCollectionView, SampleCollectionViewModel>();
            lcSet.Bind(lcSource).To(X => X.Args.Results);
            lcSet.Apply();

            CollectionView.ReloadData();

            //View = new UIView(){ BackgroundColor = UIColor.White};
            //base.ViewDidLoad();

            //// ios7 layout
            //if (RespondsToSelector(new Selector("edgesForExtendedLayout")))
            //   EdgesForExtendedLayout = UIRectEdge.None;
			   
            //var label = new UILabel(new RectangleF(10, 10, 300, 40));
            //Add(label);
            ////var textField = new UITextField(new RectangleF(10, 50, 300, 40));
            ////Add(textField);

            //var set = this.CreateBindingSet<FirstView, Core.FirstViewModel>();
            //set.Bind(label).To(vm => vm.FullName);
            ////set.Bind(textField).To(vm => vm.FullName);
            //set.Apply();
        }
    }
}