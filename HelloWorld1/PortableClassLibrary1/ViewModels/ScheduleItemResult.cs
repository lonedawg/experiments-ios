﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorld1.Core
{
	public class ScheduleItemResult
	{
		public Int16 AlertStatus { get; set; }
		public String ShortResult { get; set; }
		public String LongResult { get; set; }
	}
}
