﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld1.Core
{
	public class ScheduleItemHeader
	{
		public String Name { get; set; }
		public ScheduleItemResult LastResult { get; set; }
		public DateTime LastRan { get; set; }

		public ScheduleItemHeader(String inName, ScheduleItemResult inResult, DateTime inLastRan)
		{
			Name = inName;
			LastResult = inResult;
			LastRan = inLastRan;
		}
	}
}
