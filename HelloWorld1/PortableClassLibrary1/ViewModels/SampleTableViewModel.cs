using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace HelloWorld1.Core
{
	public class SampleTableViewModel : MvxViewModel
    {
		public ScheduleUpdatedEventArgs Args { get; set; }

		public SampleTableViewModel()
        {
			var lcClient = new HttpClient();
            var lcResult = lcClient.GetStringAsync("http://brock.in:8585/heartbeat/api/testevent/getlatest").Result;

            Args = JsonConvert.DeserializeObject<ScheduleUpdatedEventArgs>(lcResult);
        }
    }
}
