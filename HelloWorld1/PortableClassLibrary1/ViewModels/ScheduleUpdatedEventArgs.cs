﻿using System;
using System.Collections.Generic;

namespace HelloWorld1.Core
{
	public class ScheduleUpdatedEventArgs
	{
		public List<ScheduleItemHeader> Results { get; set;}
	}
}

